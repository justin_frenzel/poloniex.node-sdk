# Poloniex Node.js SDK #

This is an **UNOFFICIAL** (unaffiliated by Poloniex, Inc.) node.js package for communicating with the Public and Trading APIs of the Poloniex exchange. https://poloniex.com/support/api/

There are already a few different Poloniex wrappers for Node.JS on NPM, however I wanted to build this one, because the others that I could find did not take into account the limitation of 6 API calls per second that Poloniex enforces for the trading API. This package will limit (per instance of the Poloniex class) the number of active API calls that can be made during a given second to 5.

This package does not yet support the Poloniex Push API.

## Installation ##

This driver is available as an NPM package, it doesn't have any dependencies so it should be fairly simple.

`npm install poloniex-sdk`

Once installed, it can be used within your node.js project.

## How to Use ##

```js
const Poloniex = require("poloniex-sdk");
var poloniex = new Poloniex;
```

It does not need to be instantiated with any properties unless you intend to work with the Trading API, which will require you to get an API Key and Secret from Poloniex for your exchange account. I recommend keeping your Polonix API Key or Secret defined as environment variables so they don't accidentally get committed as part of your project's history.

```js
const Poloniex = require("poloniex-sdk");

const MY_APIKEY = process.env.poloniex_apikey || "";
const MY_SECRET = process.env.poloniex_secret || "";

var poloniex = new Poloniex(MY_APIKEY, MY_SECRET);
```

## Public API ##

You can make calls to the Public API without using a Poloniex API Key or Secret. All supported methods are accessible via the "public" property of the Poloniex object.

```js
const Poloniex = require("poloniex-sdk");
var poloniex = new Poloniex;

//This method returns a list of all the current tickers by currency pair.
poloniex.public.returnTicker((err, data) => {
	if (!err) { //'data' will have all the ticker datas returned from the API.
		console.log(data);
	}
	else //an error occurred.
		throw new Error(err.error);
		
});
```

## Trading API ##

You must pass your API Key and Secret to the Poloniex object when creating it in order to make use of the Trading API. All Trading API methods are accessible via the "private" property of the Poloniex object. Each method takes in up to two parameters, the first is data to pass for that method, the last is the callback.

```js
const Poloniex = require("poloniex-sdk");
var poloniex = new Poloniex(MY_APIKEY, MY_SECRET);

//This method will attempt to make a buy.
poloniex.trading.buy({
	currencyPair: "BTC_XRP",
	amount: 100,
	rate: '0.00010200'
}, (err, result) => {
	if (!err) { //buy was successful, 'result' has the returned order number and resulting trades.
		console.log(result);
	}
	else  //an error occurred.
		throw new Error(err.error);
		
})'
```

Some methods do not require any data to be passed, so the callback can be first in those cases. The Poloniex API documentation covers which values to pass for a given API call: https://poloniex.com/support/api/

Additionally, you can dump a list of the available API calls and which properties they take in (with TRUE indicating a required field, and FALSE indicating an optional field).

```js
const Poloniex = require("poloniex-sdk");
var poloniex = new Poloniex(MY_APIKEY, MY_SECRET);
console.log(poloniex._tradingAPIs);
```

## MIT License ##
Copyright (c) 2017 Justin Frenzel

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.

## Support the Project ##

The git repository for this project is available on Bitbucket: https://bitbucket.org/justin_frenzel/poloniex.node-sdk .

If you have any suggestions or would like to work on improving the project, feel free to submit a pull request or send me an email or something. Feel free to fork it if you'd like.

If you want to make a donation, feel free to send BTC to this address: 1ENeXQyH25NQqgNWPHvXduh92k1TNFnCc9

## Author ##

* Justin Frenzel
* justin.frenzel@gmail.com