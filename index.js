var https = require('https');
var crypto = require("crypto");
var qs = require("querystring");
var fs = require("fs");
var path = require('path');

/*
@Poloniex: Creates an API Wrapper for Poloniex, useful for both public and trading calls to their API.
arguments: 
	apikey: Your trading trading api key.
	secret: Your trading trading secret.
*/
Poloniex = function (apikey, secret) {
	var self = this;

	if (apikey)
		self.apikey = apikey;
	if (secret)
		self.secret = secret;

	self.version = require('./package.json').version;
	self.lastNonceUsed = null;

	//Poloniex does not allow for more than 6 api calls to be made per second, or they'll temporarily blacklist your IP. 
	self._requestFactory = {
		intervalLength: 1000,
		concurrency: 1,
		active: 0,
		queue: []
	};

	//This request factory is a FIFO queue which processes the requests in order, with more than 5 being executed per second.
	self._requestFactory.interval = setInterval(() => {
		while (self._requestFactory.queue.length > 0 && self._requestFactory.active < self._requestFactory.concurrency) {
			var request = self._requestFactory.queue.shift();
			self._requestFactory.active++;
			self._request(request.options, (err, response, body) => {
				self._requestFactory.active--;
				if (typeof request.callback !== "undefined")
					request.callback(err, response, body);
			});
		}
	}, self._requestFactory.intervalLength);

	self.public = {};
	self.trading = {};

	//For ease of use, assign all the apis to a "public" property of the wrapper.
	for (var i in self._publicAPIs) {
		((api) => {
			self.public[api] = (callback) => {self._call(api, callback)}
		})(self._publicAPIs[i]);
	}
	
	//For ease of use, assign all the apis to a "trading" property of the wrapper.
	for (var i in self._tradingAPIs) {
		((api) => {
			self.trading[api] = (data, callback) => {self._call(api, data, callback)}
		})(i);
	}
}

Poloniex.prototype.apikey = null;
Poloniex.prototype.secret = null;

Poloniex.prototype._publicAPI = "https://poloniex.com/public";
Poloniex.prototype._tradingAPI = "https://poloniex.com/tradingApi";

//This is a list of the available public APIs. None requre any post data, and are publicly accessible.
Poloniex.prototype._publicAPIs = JSON.parse(fs.readFileSync(__dirname + "/apis/public.json"));

//This is a list of the available trading APIs. The API is "null" if there is no post data required. Otherwise, the required fields are "TRUE", optional fields are "FALSE". All require an non-null apikey and secret.
Poloniex.prototype._tradingAPIs = JSON.parse(fs.readFileSync(__dirname + "/apis/trading.json"));

Poloniex.prototype._request = function _request (options, callback) {
	var self = this;

	options.hostname = options.url.split("https://")[1].split("/")[0];
	options.path = options.url.split(options.hostname)[1];
	options.port = null;

	options.body.nonce = (self.lastNonceUsed) ? ++self.lastNonceUsed : Date.now();
	self.lastNonceUsed = options.body.nonce;

	var dataString = qs.stringify(options.body);

	if (self.apikey && self.secret) {
		options.headers.Key = self.apikey;
		options.headers.Sign = crypto.createHmac("sha512", self.secret).update(dataString).digest("hex");
	}

	var request = https.request(options, (response) => {
		var chunks = [];
		response.on("data", (chunk) => {
			chunks.push(chunk);
		});

		response.on("error", (err) => {
			callback(err, response, null);
		})

		response.on("end", () => {
			var body = Buffer.concat(chunks);
			callback(null, response, body.toString("utf-8"));
		});
	});

	if (options.method.toUpperCase() == "POST" && options.body != null) {
		request.write(dataString);
		request.end();
	}
	else
		request.end();
}

Poloniex.prototype._call = function (api, postData, callback) {
	var self = this;

	if (!callback) {
		var callback = postData;
		postData = {};
	}
	
	if (self._publicAPIs.indexOf(api) !== -1) {
		//is a public API call.
		var url = self._publicAPI + "?command=" + api;
		var method = "GET";
	}
	else if (typeof self._tradingAPIs[api] !== "undefined") {
		var url = self._tradingAPI;
		var method = "POST";
		postData.command = api;
		//is a trading API call.
		if (!self.apikey || !self.secret)
			throw new Error("The Poloniex API method '" + api + "' is a private Trading API method. You must specify an 'apikey' and 'secret' within the Poloniex object options to make a trading API call.");

		if (self._tradingAPIs[api] != null) {
			for (var i in self._tradingAPIs[api]) {
				if (self._tradingAPIs[api][i] == true && typeof postData[i] == "undefined")
					throw new Error("The Poloniex API method '" + api + "' requires the post data parameter '" + i + "' to be performed.");
			}
		}
	}
	else
		throw new Error("The API method '" + api + "' does not exist. Please try an API supported by Poloniex's Public or Trading APIs. https://poloniex.com/support/api/");

	var options = {
		url: url,
		method: method,
		headers: {
			"User-Agent": "https://www.npmjs.com/package/poloniex-sdk@" + self.version,
			"Content-Type": "application/x-www-form-urlencoded",
			"Accept": "application/json"
		},
		body: postData
	}

	var cb = function (error, response, body) {
		if (typeof callback == "function") {
			if (!error) {
				try {
					var data = JSON.parse(body);
				}
				catch (err) {
					var data = body;
				}

				if (response.statusCode < 300 && typeof data.error == "undefined")
					callback(null, response.statusCode == 204 ? true : data);
				else
					callback(data, null);
			}
			else
				callback(error, null);
		}
	}

	self._requestFactory.queue.push({options: options, callback: cb});
}

module.exports = Poloniex;