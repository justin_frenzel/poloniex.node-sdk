const Poloniex = require("./index.js");

const MY_APIKEY = "";
const MY_SECRET = "";

var poloniex = new Poloniex(MY_APIKEY, MY_SECRET);

//This method returns a list of all the current tickers by currency pair.
poloniex.public.returnTicker((err, data) => {
	if (!err) { //'data' will have all the ticker datas returned from the API.
		console.log(data);
	}
	else //an error occurred.
		throw new Error(err.error);
		
});

//This method will attempt to make a buy.
poloniex.trading.buy({
	currencyPair: "BTC_DOGE",
	amount: 1000,
	rate: '0.0000000100'
}, (err, result) => {
	if (!err) { //buy was successful, 'result' has the returned order number and resulting trades.
		console.log(result);
	}
	else  //an error occurred.
		throw new Error(err.error);
});